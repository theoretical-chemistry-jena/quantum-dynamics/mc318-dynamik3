#!/usr/bin/env python3

import argparse
from pathlib import Path
import sys

import matplotlib as mpl
import matplotlib.animation as animation
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import numpy as np


mpl.rcParams["font.size"] = 14
mpl.rcParams["legend.fontsize"] = 10
mpl.rcParams["lines.linewidth"] = 2


def parse_args(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("--out", default="out", type=str)
    parser.add_argument("--save", action="store_true")

    return parser.parse_args(args)


def run():
    args = parse_args(sys.argv[1:])
    out = Path(args.out)

    Nx = Ny = 128 // 2
    frames = 3000
    every_psi = 10
    frames_psi = frames // every_psi

    ################
    # Loading data #
    ################

    # Electronic potentials
    gs, es = [
        np.loadtxt(out / fn).reshape(Nx, Ny, -1)
        for fn in ("Ground_Pot.dat", "Excited_Pot.dat")
    ]

    # Spatial expectation values
    R_gs, R_es = [np.loadtxt(out / fn) for fn in ("xy_g.dat", "xy_a.dat")]
    _, R_gs_x0, R_gs_y0 = R_gs[0]
    _, R_es_x0, R_es_y0 = R_es[0]
    assert frames == R_gs.shape[0]

    # Lasers, pew pew pew
    laser1, laser2 = [np.loadtxt(out / fn) for fn in ("laser1.dat", "laser2.dat")]

    # We have to drop some frames from <R> and the lasers, so their number matches
    # the number of available |Psi²| values.
    R_gs = R_gs[::every_psi]
    R_es = R_es[::every_psi]
    laser1 = laser1[::every_psi]
    laser2 = laser2[::every_psi]

    psi2_gs = np.loadtxt(out / "abs_psi_g2.dat").reshape(frames_psi, 64, 64, 3)
    psi2_es = np.loadtxt(out / "abs_psi_a2.dat").reshape(frames_psi, 64, 64, 3)

    # Left/right dissociation
    t, l_diss, r_diss = np.loadtxt(out / "L_R_Dissoziation.dat").T
    t_pruned = t[::every_psi]

    # Wavefunction norms
    norm_gs, norm_es = [np.loadtxt(out / fn) for fn in ("norm_g.dat", "norm_a.dat")]

    X, Y, psi2_gs0 = psi2_gs[0].T

    tfin = laser1[-1, 0]

    # Set lowest energy to 0.0
    gs_min = gs[:, :, 2].min()
    gs[:, :, 2] -= gs_min
    es[:, :, 2] -= gs_min

    level_min = gs[:, :, 2].min()  # Zero, by definition
    level_max = gs[:, :, 2].max() * 0.125
    levels = np.linspace(level_min, level_max, 25)

    fig = plt.figure(constrained_layout=True, figsize=(16, 12))
    grid = GridSpec(ncols=2, nrows=3, figure=fig, height_ratios=[2, 1, 1])
    ax0 = fig.add_subplot(grid[0, 0])
    ax1 = fig.add_subplot(grid[0, 1])
    ax2 = fig.add_subplot(grid[1, :])
    ax3 = fig.add_subplot(grid[2, :])

    psi2_gs_levels = np.linspace(0, psi2_gs[..., -1].max(), 20)
    psi2_es_levels = np.linspace(0, psi2_es[..., -1].max(), 20)

    c_psi2_gs = ax0.contourf(X, Y, psi2_gs0, cmap="plasma", levels=psi2_gs_levels)
    c_psi2_es = ax1.contourf(
        X, Y, psi2_es[0, :, :, -1], cmap="plasma", levels=psi2_es_levels
    )
    # ax0.contour(*gs.T, levels=levels, c="white")
    gs_pes = ax0.contour(*gs.T, levels=levels)
    es_pes = ax1.contour(*es.T, levels=levels)
    ax0.set_title("GS")
    # ax1.contour(*es.T, levels=levels, c="white")
    ax1.set_title("ES")

    scatter_gs = ax0.scatter(R_gs_x0, R_gs_y0, s=50, c="red", label="<R>", zorder=7)
    scatter_es = ax1.scatter(R_es_x0, R_es_y0, s=50, c="red", label="<R>", zorder=7)

    ax1.legend()

    for ax in (ax0, ax1):
        ax.set_xlabel("x (OH)")
        ax.set_ylabel("y (OD)")

    ###############
    # Laser field #
    ###############

    ax2.set_title("Laser pew pew pew")
    scatter_laser1 = ax2.scatter(*laser1[0], s=30, c="blue")
    scatter_laser2 = ax2.scatter(*laser2[0], s=30, c="red")
    # Enable lines below if E-field should be animated
    # (line_laser1,) = ax2.plot([], [], c="blue", label="Laser 1")
    # (line_laser2,) = ax2.plot([], [], c="red", label="Laser 2")
    ax2.plot(*laser1.T, c="blue", alpha=0.5, label="Laser 1")
    ax2.plot(*laser2.T, c="red", alpha=0.5, label="Laser 2")
    ax2.axhline(0.0, ls="--", c="k")

    ax2_min = min(laser1[:, 1].min(), laser2[:, 1].min())
    ax2_min -= abs(0.1 * ax2_min)
    ax2_max = max(laser1[:, 1].max(), laser2[:, 1].max())
    ax2_max += 0.1 * ax2_max
    ax2.set_xlim(0.0, tfin)
    ax2.set_ylim(ax2_min, ax2_max)

    ax2.set_xlabel("t / fs")
    ax2.set_ylabel("E-Field")
    ax2.legend()

    #########
    # Norms #
    #########

    ax3.plot(*norm_gs.T, ls="--", label="norm GS")
    ax3.plot(*norm_es.T, ls="--", label="norm ES")
    ax3.set_xlim(0.0, tfin)
    ax3.set_xlabel("t / fs")
    ax3.set_ylabel("Norm")
    ax3.legend(loc="upper right")
    vline = ax3.axvline(0.0, c="k", ls="--")

    ax3_twin = ax3.twinx()
    ax3_twin.plot(t, l_diss, c="teal", label="Diss. L")
    ax3_twin.plot(t, r_diss, c="magenta", label="Diss. R")
    ax3_twin.set_ylabel("Dissoziation")
    ax3_twin.legend(loc="lower right")

    saved_frames = set()

    def animate(frame):
        nonlocal c_psi2_gs
        nonlocal c_psi2_es
        nonlocal gs_pes
        nonlocal es_pes

        scatter_gs.set_offsets(R_gs[frame][1:])
        scatter_es.set_offsets(R_es[frame][1:])
        scatter_laser1.set_offsets(laser1[frame])
        scatter_laser2.set_offsets(laser2[frame])
        # line_laser1.set_data(*laser1[:frame].T)
        # line_laser2.set_data(*laser2[:frame].T)
        for _ in c_psi2_gs.collections:
            _.remove()
        c_psi2_gs = ax0.contourf(
            X,
            Y,
            psi2_gs[frame, :, :, -1],
            cmap="plasma",
            levels=psi2_gs_levels,
            zorder=1,
        )

        for _ in c_psi2_es.collections:
            _.remove()
        c_psi2_es = ax1.contourf(
            X,
            Y,
            psi2_es[frame, :, :, -1],
            cmap="plasma",
            zorder=1,
            levels=psi2_es_levels,
        )

        for _ in gs_pes.collections:
            _.remove()
        gs_pes = ax0.contour(*gs.T, levels=levels)
        for _ in es_pes.collections:
            _.remove()
        es_pes = ax1.contour(*es.T, levels=levels)

        t = t_pruned[frame]
        # Save every 5 frames, but only once per run.
        if args.save and (frame % 5 == 0) and (frame not in saved_frames):
            fs = f"{t:08.3f}".replace(".", "_")
            fig.savefig(str(out / f"t_{fs}_fs_frame_{frame:03d}.png"))
            saved_frames.add(frame)

        vline.set_xdata([t, t])

        return (
            (
                scatter_gs,
                scatter_es,
                scatter_laser1,
                scatter_laser2,
                # line_laser1,
                # line_laser2,
                vline,
            )
            + tuple(c_psi2_gs.collections)
            + tuple(c_psi2_es.collections)
            + tuple(gs_pes.collections)
            + tuple(es_pes.collections)
        )

    fig.tight_layout()

    anim = animation.FuncAnimation(
        fig,
        animate,
        frames_psi,
        interval=5,
        blit=True,
    )

    plt.show()


if __name__ == "__main__":
    run()
