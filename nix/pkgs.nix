let
  sources = import ./sources.nix;
  pkgs = import sources.nixpkgs {
    overlays = [ ];
    config = {
      allowUnfree = true;
      allowUnsupportedSystem = true;
    };
  };
in pkgs
