FC  		= gfortran
FFLAGS		= -O3 -w -fopenmp -march=native ${FFTWFLGS}
LDFLAGS 	= -lm
FFTWFLGS	=  -I${FFTW_IDIR} -L${FFTW_LDIR} -lfftw3

FFTW_DIR    ?= /usr
FFTW_IDIR   ?= $(FFTW_DIR)/include
FFTW_LDIR   ?= $(FFTW_DIR)/lib64

default: 	dynamik3

dynamik3:   	data_au.o laser.o main.o propagation.o
	${FC} *.o ${FFLAGS} ${LDFLAGS} -o dynamik3

main.o:	 src/main.f90
	${FC} $< ${FFLAGS} -c -o $@

laser.o:	 src/laser.f90
	${FC} $< ${FFLAGS} -c -o $@

data_au.o: src/data_au.f90
	${FC} $< ${FFLAGS} -c -o $@

propagation.o:	src/propagation.f90
	${FC} $< ${FFLAGS} -c -o $@

clean:
	rm -f *.o
	rm -f *.mod
	rm -f dynamik3
	rm -f movieout
	rm -f time

