
let
  pkgs = import ./nix/pkgs.nix;
  pythonEnv = pkgs.python3.withPackages(ps: with ps; [
    numpy
    scipy
    matplotlib
  ]);

in
  with pkgs; mkShell {
    name = "MC3.1.8";
    buildInputs = [

      # Basic software
      which
      git

      # Scientific software
      gfortran
      gnuplot
      fftw
      fprettify

      # Misc
      imagemagick
      gifsicle

      pythonEnv
    ];

    shellHook = ''
      export FFTW_DIR=${fftw.dev}
    '';
  }
