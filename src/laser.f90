module laser

   use iso_fortran_env, only: real64
   use data_au, only: cm2au, fs2au

   implicit none

   type :: t_laser
      real(kind=real64) :: wavelength  ! Wavelength; expected in nm.
      real(kind=real64) :: center  ! Pulse center; expected in fs.
      real(kind=real64) :: length  ! Pulse length; expected in fs.

      ! Calculated from the values above in the constructor.
      real(kind=real64) :: fwhm  ! Full width at half maximum.
      real(kind=real64) :: omega  ! Photon energy in Hartree.

   contains
      procedure :: field
      procedure :: info
   end type t_laser

   interface t_laser
      module procedure :: t_laser_constructor
   end interface t_laser

contains

   pure type(t_laser) function t_laser_constructor( &
      wavelength, center, length) result(res)
      ! Calculate FWHM and omega from provided length and wavelength.

      real(kind=real64), intent(in) :: wavelength
      real(kind=real64), intent(in) :: center
      real(kind=real64), intent(in) :: length
      real(kind=real64) :: fwhm, omega

      res%wavelength = wavelength
      res%center = center*fs2au
      res%length = length*fs2au
      res%fwhm = 4.0*log(2.0)/(res%length**2)
      res%omega = 1.0/(res%wavelength*1d-7)*cm2au

   end function t_laser_constructor

   pure complex function field(self, time)
      ! Laser field at given time in atomic units

      class(t_laser), intent(in) :: self
      real(kind=real64), intent(in) :: time

      field = exp(-self%fwhm*(time - self%center)**2 - (0.0, 1.0)*self%omega*time)

   end function field

   subroutine info(self)
      ! Print short summary of laser parameters
      class(t_laser), intent(in) :: self

      write (*, "(A, F8.2, A)"), "Wellenlänge: ", self%wavelength, " nm"
      write (*, "(A, F8.2, A)"), "    Zentrum: ", self%center/fs2au, " fs"
      write (*, "(A, F8.2, A)"), "      Länge: ", self%length/fs2au, " fs"

   end subroutine info

end module laser
