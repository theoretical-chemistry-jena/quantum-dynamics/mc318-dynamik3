module data_au
   double precision, parameter:: au2a = 0.52917706d0  ! Umrechnungsfaktor Laenge in a.u. --> Angstrom
   double precision, parameter:: cm2au = 4.5554927d-6 ! Umrechnungsfaktor Wellenzahlen cm⁻¹ -> Hartree
   double precision, parameter:: fs2au = 41.341374575751
   double precision, parameter:: au2fs = 1.0/fs2au
   double precision, parameter:: j2eV = 6.242D18      ! Umrechnungsfaktor Energie von J --> eV
   double precision, parameter:: au2eV = 27.2116d0    ! Umrechnungsfaktor Energie von a.u. --> eV
   double precision, parameter:: i2au = 2.0997496D-9
   double precision, parameter:: pi = 3.141592653589793d0    ! einfach nur pi
   double precision, parameter:: d2au = 0.3934302014076827d0 ! Umrechnungsfaktor Dipolmoment von Debye --> a.u.
   double precision, parameter:: amu = 1822.888d0    ! Atomic mass unit (atomare Masseneinheit)
   double precision, parameter:: m_h = 1.d0     ! reduzierte Masse eines H atoms
   double precision, parameter:: m_d = 2.d0     ! reduzierte Masse eines D atoms
   complex*16, parameter:: im = (0.d0, 1.d0)      ! Das ist i, die imaginaere Zahl
end module
