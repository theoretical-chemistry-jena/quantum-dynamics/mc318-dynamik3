subroutine propagation(psi0)

   use data_grid
   use pot_param
   use data_au
   use laser, only: t_laser
   use omp_lib
   use, intrinsic :: iso_c_binding

   implicit none

   include 'fftw3.f03'

   integer I, J, K, L

   type(C_PTR):: planFA, planFG, planBA, planBG
   integer*8:: void

   character*32:: datei                        ! Fuer die Filmerstellung
   double precision:: tts, tte                 ! nun - die Zeit
   double precision:: time                     ! immer noch die Zeit
   double precision:: x, y                     ! Kernabstand
   double precision:: cpm                      ! Abschneideparameter
   double precision:: norm_g, norm_a           ! die Gesamtnorm der Wellenfunktion
   double precision:: norm_g2, delta_raus      ! die Norm der Wellenfunktion, Dissoziation
   double precision:: evx_g, evy_g             ! Ortserwartungswert, Grundzustand
   double precision:: evx_a, evy_a             ! Ortserwartungswert, angeregter Zustand
   double precision:: raus_x, raus_y           ! Wieviel in x- und y-Richtung rauslaeuft
   double precision:: cofx(nx), cofy(ny)       ! Abschneidefunktion
   double precision, intent(in):: psi0(nx, ny) ! die Grundzustandswellenfunktion

   complex:: field1
   complex:: field2

   double precision, allocatable, dimension(:):: densx_g, densy_g  ! Dichte
   double precision, allocatable, dimension(:):: densx_a, densy_a  ! Dichte
   complex*16, allocatable, dimension(:):: psi, psi2       ! Fourier Hilfsgroesse
   complex*16, allocatable, dimension(:, :):: psi_A        ! Wellenfunktion, angeregter Zustand
   complex*16, allocatable, dimension(:, :):: psi_G        ! Wellenfunktion, Grundzustand
   complex*16, allocatable, dimension(:, :):: psi_sam      ! Aufsammelnde Funktion
   complex*16, allocatable, dimension(:, :):: kprop        ! Kinetischer Propagator
   complex*16, allocatable, dimension(:, :):: vprop_a      ! Potentieller Propagator, angeregter Zustand
   complex*16, allocatable, dimension(:, :):: vprop_g      ! Potentieller Propagator, Grundzustand

   open (101, file='out/cut_off_function.dat', status='unknown')   ! Die Abschneidefunktion - 2D
   open (200, file='out/dens_x_a.dat', status='unknown')           ! Kernwellenpaket im oberen Zustand - 3D
   open (201, file='out/dens_x_g.dat', status='unknown')           ! Kernwellenpaket im unteren Zustand - 3D
   open (202, file='out/dens_y_a.dat', status='unknown')           ! Dichte im oberen Zustand - 3D
   open (203, file='out/dens_y_g.dat', status='unknown')           ! Dichte im unteren Zustand - 3D
   open (300, file='out/laser1.dat', status='unknown')             ! Laserfeld des anregenden Lasers - 2D
   open (301, file='out/laser2.dat', status='unknown')             ! Laserfeld des ab Lasers - 2D
   open (800, file='out/xy_a.dat', status='unknown')               ! Ortserwartungswerte - drei Spalten
   open (801, file='out/xy_g.dat', status='unknown')               ! Ortserwartungswerte - drei Spalten
   open (908, file='out/norm_a.dat', status='unknown')             ! Norm im el. Zustand- 2D, zwei Spalte
   open (909, file='out/norm_g.dat', status='unknown')             ! Norm im el. Zustand- 2D, zwei Spalte
   open (900, file='out/L_R_Dissoziation.dat', status='unknown')   ! Dissoziationskanal - 2D, drei Spalten
   open (901, file='out/abs_psi_g2.dat', status='unknown')         ! Betragsquadrat Psig
   open (902, file='out/abs_psi_a2.dat', status='unknown')         ! Betragsquadrat Psia

   allocate (psi_a(nx, ny), kprop(nx, ny), vprop_g(nx, ny), psi_sam(nx, ny))
   allocate (psi_g(nx, ny), vprop_a(nx, ny), psi(ny), psi2(nx))
   allocate (densx_g(nx), densx_a(nx), densy_g(ny), densy_a(ny))

   call dfftw_plan_dft_2d(planFA, nx, ny, psi_a, psi_a, FFTW_FORWARD, FFTW_MEASURE)
   call dfftw_plan_dft_2d(planFG, nx, ny, psi_g, psi_g, FFTW_FORWARD, FFTW_MEASURE)
   call dfftw_plan_dft_2d(planBA, nx, ny, psi_a, psi_a, FFTW_BACKWARD, FFTW_MEASURE)
   call dfftw_plan_dft_2d(planBG, nx, ny, psi_g, psi_g, FFTW_BACKWARD, FFTW_MEASURE)

   psi_a = (0.d0, 0.d0)                    ! Initialisierung
   psi_g = (0.d0, 0.d0)
   psi = (0.d0, 0.d0)
   psi2 = (0.d0, 0.d0)

   raus_x = 0.d0
   raus_y = 0.d0

   cpm = 1.5d0/au2a

   do j = 1, Nx                            ! Das ist eine Abschneidefunktion, die verhindern soll,
      x = x0 + (j - 1)*dx                 ! dass ein Wellenpaket, wenn es dissoziiert, an das Ende
      if (x .lt. (xend - cpm)) then       ! des Grids stoesst.
         cofx(j) = 1.d0                  ! Daher schneidet diese Funktion (cut-off-function) die
      else                                ! auslaufende Wellenfunktion sanft ein paar Angstrom
         cofx(j) = cos(((x - xend + cpm)/-cpm)*(0.5d0*pi))   ! (definiert durch den
         cofx(j) = cofx(j)**2                                ! Parameter CPM) vorher ab.
      end if
      write (101, *) sngl(x*au2a), sngl(cofx(j))
   end do

   do j = 1, Ny
      y = y0 + (j - 1)*dy
      if (y .lt. (yend - cpm)) then
         cofy(j) = 1.d0
      else
         cofy(j) = cos(((y - yend + cpm)/-cpm)*(0.5d0*pi))
         cofy(j) = cofy(j)**2
      end if
   end do

   do i = 1, Nx
      do j = 1, Ny
         kprop(i, j) = exp(-im*dt*(Px(i)**2/(2.d0*mass_h) +& ! Kinetischer Propagator
            & Py(j)**2/(2.d0*mass_d)))
         vprop_a(i, j) = exp(-0.5d0*im*dt*pot(i, j, 2)) ! Potentieller Propagator - angeregter
         vprop_g(i, j) = exp(-0.5d0*im*dt*pot(i, j, 1))
      end do
   end do

!______________________________________________________________________
!
!                   Propagation Loop
!_____________________________________________________________________

   print *
   print *, '2D propagation...'
   print *

   tts = omp_get_wtime()

   timeloop: do K = 1, Nt                        ! Zeitschleife

      time = K*dt

      if (mod(K, 100) .eq. 0) then                ! Schreibt alle 100 Werte die Zeit
         print *, 'time:', sngl(time*au2fs)             ! auf den Bildschirm
      end if

!============== Definition der Laserfelder ====================================

      field1 = laser1%field(time)
      field2 = laser2%field(time)

! ============= Propagationsteil ==============================================
!
! Die Propagation findet dieses Mal fuer die Wellenfunktionen in beiden
! elektronischen Zustaenden, im angeregten und wieder im Grundzustand statt.
! _____________________________________________________________________________

    psi_a = psi_a + dt*psi0*field1  ! Anregung, 1. Ordnung zeitabh. Stoerungstheorie
    psi_g = psi_g + dt*psi_a*field2  ! Abregung, 2. Ordnung zeitabh. Stoerungstheorie

      ! ......Potentielle Propagation ........................

      psi_a = psi_a*vprop_a                                ! Angeregter Zustand
      psi_g = psi_g*vprop_g                                ! Grundzustand

      ! ......Kinetische Propagation ....................

      ! Grundzustand (siehe planFG)
      call fftw_execute_dft(planFG, psi_g, psi_g)      ! Fouriertransformation: Psi(R) --> Psi(P)

      psi_g = psi_g*kprop                          ! Kinetische Propagation

      call fftw_execute_dft(planBG, psi_g, psi_g)      ! Fouriertransformation: Psi(R) --> Psi(P)

      ! Angeregter Zustand (siehe planFA)
      call fftw_execute_dft(planFA, psi_a, psi_a)      ! Fouriertransformation: Psi(R) --> Psi(P)

      psi_a = psi_a*kprop

      call fftw_execute_dft(planBA, psi_a, psi_a)      ! Fouriertransformation: Psi(R) --> Psi(P)

      ! Renormierung
      psi_g = psi_g/dble(nx*ny)        ! Angeregter Zustand
      psi_a = psi_a/dble(nx*ny)        ! Angeregter Zustand

      ! ......Potentielle Propagation ........................

      psi_a = psi_a*vprop_a                                ! Angeregter Zustand
      psi_g = psi_g*vprop_g                                ! Grundzustand

! ============ Ende der Propagation, hier kommt nur noch Output ==============

      call integ(psi_a, norm_a)                        ! Berechnet die Norm im angeregten Zustand
      call integ(psi_g, norm_g)                        ! Berechnet die Norm im Grundzustand

      call density(psi_g, densx_g, densy_g, evx_g, evy_g)  ! Berechnet Dichte und Erwartungswerte
      call density(psi_a, densx_a, densy_a, evx_a, evy_a)

      write (800, *) sngl(time*au2fs), sngl(evx_a*au2a), sngl(evy_a*au2a)! Schreibt Ortserwartungswerte raus
      write (801, *) sngl(time*au2fs), sngl(evx_g*au2a), sngl(evy_a*au2a)! Schreibt Ortserwartungswerte raus
      write (908, *) sngl(time*au2fs), sngl(norm_a)          ! Schreibt die Population im angeregt. Zustand
      write (909, *) sngl(time*au2fs), sngl(norm_g)          ! Schreibt die Population im Grundzustand
      write (300, *) sngl(time*au2fs), sngl(real(field1))  ! Schreibt das Laserfeld raus
      write (301, *) sngl(time*au2fs), sngl(real(field2))  ! Schreibt das Laserfeld raus

      if (mod(k, 20) .eq. 0) then
         do l = 1, Ny, 2
            y = y0 + (l - 1)*dy
            write (201, *) sngl(time*au2fs), sngl(y*au2a), sngl(densy_g(l))
            write (200, *) sngl(time*au2fs), sngl(y*au2a), sngl(densy_a(l))
         end do
         do l = 1, Nx, 2
            x = x0 + (l - 1)*dx
            write (203, *) sngl(time*au2fs), sngl(x*au2a), sngl(densx_g(l))
            write (202, *) sngl(time*au2fs), sngl(x*au2a), sngl(densx_a(l))
         end do
         write (200, *)
         write (201, *)
         write (202, *)
         write (203, *)
      end if

! ======== Hier wird die gif-Animation erstellt ==================

      if (movieparam .eq. 1) then

         if (mod(k, 10) .eq. 0) then

            open (10, file="out/dens_tmp.out")
            do i = 1, nx, 2
               do j = 1, ny, 2
                  x = (x0 + (i - 1)*dx) * au2a
                  y = (y0 + (j - 1)*dy) * au2a
                  write (10, *) sngl(x), sngl(y), sngl(abs(Psi_a(i, j))**2)
                  write (901, *) sngl(x), sngl(y), sngl(abs(Psi_g(i, j))**2)
                  write (902, *) sngl(x), sngl(y), sngl(abs(Psi_a(i, j))**2)
               end do
               write (10, *)
               write (901, *)
               write (902, *)
            end do
            close (10)

            open (10, file="time")
            write (10, 902) 'set title "Zeit: ', int(time*au2fs), 'fs"'
902         format(A36, I3.3, A5)
            close (10)

            open (10, file="out/run_gnu.sh")
            write (10, *) "cat time moviescript > movieout"
            write (10, *) "gnuplot movieout"
            write (datei, 901) "out/mov/d_", k, ".gif"
            write (10, *) "convert -rotate 0 dens_movie.png ", datei
            close (10)
            call system("sh out/run_gnu.sh")

         end if
      end if

!======================================================================

! ........... Berechnung des rauslaufenden Teils ..............

      psi_sam = (0.d0, 0.d0)
      delta_raus = 0.d0

      do I = 1, nx
         psi_sam(i, :) = psi_g(i, :)*cofx(i)
      end do

      call integ(psi_sam, norm_g2)
      delta_raus = norm_g - norm_g2
      raus_x = raus_x + delta_raus

      psi_sam = (0.d0, 0.d0)
      delta_raus = 0.d0

      do I = 1, ny
         psi_sam(:, i) = psi_g(:, i)*cofy(i)
      end do

      call integ(psi_sam, norm_g2)
      delta_raus = norm_g - norm_g2
      raus_y = raus_y + delta_raus

      write (900, *) sngl(time*au2fs), sngl(raus_x), sngl(raus_y)         ! Schreibt den rauslaufenden Teil raus

      do i = 1, nx
         do j = 1, ny
            psi_g(i, j) = psi_g(i, j)*cofx(i)*cofy(j)
         end do
      end do

   end do timeloop                                        ! Ende der Zeitschleife

   tte = omp_get_wtime()

   write (*, *) "Time to finish: ", tte - tts, "s"

901 format(A9, I5.5, A4)

   close (101, status='keep')
   close (200, status='keep')
   close (201, status='keep')
   close (202, status='keep')
   close (203, status='keep')
   close (300, status='keep')
   close (301, status='keep')
   close (800, status='keep')
   close (908, status='keep')
   close (909, status='keep')
   close (900, status='keep')
   close (901, status='keep')
   close (902, status='keep')

   deallocate (psi_a, kprop, vprop_a, psi_g, vprop_g)
   deallocate (densx_g, densx_a, densy_g, densy_a)

   return
end subroutine

!_________________________________________________________

subroutine integ(psi, norm)

   use data_grid

   implicit none
   integer:: i, j
   double precision, intent(out):: norm
   complex*16, intent(in):: psi(Nx, ny)

! Dieses Unterprogramm rechnet die Norm der komplexen Wellenfunktion aus.

   ! norm = 0.d0

   ! do I = 1, Nx
   !     do j = 1, Ny
   !         norm = norm + abs(psi(i, j))**2
   !     end do
   ! end do

   norm = sum(abs(conjg(psi)*psi))*dx*dy

   return
end subroutine

!_________________________________________________________

subroutine density(psi, densx, densy, evx, evy)

   use data_grid
   use pot_param

   implicit none
   integer:: I, J
   double precision:: x, y, norm
   double precision, intent(out):: densx(Nx), densy(Ny), evx, evy
   complex*16, intent(in):: psi(Nx, Ny)

   densy = 0.d0                ! Dichte von Elektron y,  rho(y,t)
   densx = 0.d0                ! Dichte von Elektron x,  rho(x,t)
   evx = 0.d0                  ! Ortserwartungswert in x,  <x(t)>
   evy = 0.d0                ! Ortserwartungswert in y,  <y(t)>

!... In dieser Subroutine werden Elektronendichten und Ortserwartungswerte ausgerechnet..
   do I = 1, Ny
      do J = 1, Nx
         densy(I) = densy(I) + abs(psi(I, J))**2   ! rho(y) = int |psi(x,y)|^2 dx
      end do
      densy(I) = densy(I)*dx
   end do

   do J = 1, Nx
      do I = 1, Ny
         densx(J) = densx(J) + abs(psi(I, J))**2        ! rho(x) = int |psi(x,y)|^2 dy
      end do
      densx(J) = densx(J)*dy
   end do

   call integ(psi, norm)

   do I = 1, Ny
      y = y0 + (I - 1)*dy
      evy = evy + dble(y*densy(I))        ! <y(t)> = int |psi(x,y)|^2 y dx dy
   end do
   evy = evy*dy/norm

   do J = 1, Nx
      x = x0 + (J - 1)*dx
      evx = evx + dble(x*densx(J))        ! <x(t)> = int |psi(x,y)|^2 x dx dy
   end do
   evx = evx*dx/norm

   return
end subroutine

! __________________________ Fourier-Transformationsroutine________

! COMPLEX FAST FOURIER TRANSFORMATION, NN must be a power of 2
! ISIGN = -1 : x -> p
! ISIGN = +1 : p -> x

SUBROUTINE DFFT1(DATAS, NN, ISIGN)
   implicit REAL*8(a - h, o - z)
   DIMENSION DATAS(*)

   N = 2*NN
   J = 1

   DO 11 I = 1, N, 2
      IF (J .GT. I) THEN
         TEMPR = DATAS(J)
         TEMPI = DATAS(J + 1)
         DATAS(J) = DATAS(I)
         DATAS(J + 1) = DATAS(I + 1)
         DATAS(I) = TEMPR
         DATAS(I + 1) = TEMPI
      END IF
      M = N/2
1     IF ((M .GE. 2) .AND. (J .GT. M)) THEN
         J = J - M
         M = M/2
         GO TO 1
      END IF
      J = J + M
11    CONTINUE
      MMAX = 2
2     IF (N .GT. MMAX) THEN
         ISTEP = 2*MMAX
         THETA = 6.28318530717959D0/(ISIGN*MMAX)
         WPR = -2.D0*DSIN(0.5D0*THETA)**2
         WPI = DSIN(THETA)
         WR = 1.D0
         WI = 0.D0
         DO 13 M = 1, MMAX, 2
            DO 12 I = M, N, ISTEP
               J = I + MMAX
               TEMPR = WR*DATAS(J) - WI*DATAS(J + 1)
               TEMPI = WR*DATAS(J + 1) + WI*DATAS(J)
               DATAS(J) = DATAS(I) - TEMPR
               DATAS(J + 1) = DATAS(I + 1) - TEMPI
               DATAS(I) = DATAS(I) + TEMPR
               DATAS(I + 1) = DATAS(I + 1) + TEMPI
12             CONTINUE
               WTEMP = WR
               WR = WR*WPR - WI*WPI + WR
               WI = WI*WPR + WTEMP*WPI + WI
13             CONTINUE
               MMAX = ISTEP
               GO TO 2
               END IF
               RETURN
            END

!********************************************************************
