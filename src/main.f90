module data_grid
   use laser, only: t_laser

   integer:: Nt           ! Anzahl der Zeitschritte
   integer:: movieparam         ! Parameter, der definiert, ob ein Film erstellt wird
   integer, parameter:: Nx = 128       ! Anzahl der Punkte des Ortsgrids
   integer, parameter:: Ny = 128       ! Anzahl der Punkte des Ortsgrids
   double precision:: dx, dpx, dt       ! Schrittgroesse Orts-, Impuls- und Zeitgrid
   double precision:: dy, dpy       ! Schrittgroesse Orts-, Impuls- und Zeitgrid
   double precision:: Px(nx), py(ny)     ! Impulsgrid
   double precision:: Pot(nx, ny, 2)     ! Die Potentialkurven
   double precision:: mass_h, mass_d       ! Die Masse des Systems
   double precision:: t_1, t_2         ! Zentren der beiden Pulse
   type(t_laser) :: laser1, laser2  ! Lasers, pew pew pew
end module

module pot_param
   use data_au
   double precision, parameter:: x0 = 0.2d0     ! Ortsgrid Parameter, Anfang..
   double precision, parameter:: xend = 8.2d0   ! ..und Ende
   double precision, parameter:: y0 = 0.20d0     ! Ortsgrid Parameter, Anfang..
   double precision, parameter:: yend = 8.2d0   ! ..und Ende
end module pot_param

program wavepacket

   use data_grid
   implicit none

   double precision, allocatable, dimension(:, :):: psi0

   print *
   print *
   print *, 'Initialization...'
   print *

   call system('mkdir -p out')
   call system("cp input out/")

   call input
   call p_grid

   allocate (psi0(nx, ny))

   call potential
   call nuclear_wavefct(psi0)
   call propagation(psi0, laser1, laser2)

   print *, 'Finished'
   deallocate (psi0)

end program

! _______________ Subroutines __________________________________________________

subroutine input

   use data_grid
   use pot_param
   implicit none

   double precision:: lambda1, lambda2
   double precision:: tp1, tp2
   double precision:: x_eq

   open (10, file='input', status='old')

! In diesem Unterprogramm werden Parameter eingelesen und in atomare Einheiten
! umgerechnet.
! Parameter: Laserparameter

   read (10, *) Nt            ! Nt = number of time steps.
   read (10, *) lambda1        ! = Wellenlaenge des Anregungslasers in nm
   read (10, *) lambda2      ! = Wellenlaenge des Abfragelasers in nm
   read (10, *) tp1, tp2      ! = Laenge Laserpuls 1, Laenge Laser 2 in fs
   read (10, *) t_1, t_2      ! = Zentren der Laserpulse 1 & 2 in fs
   read (10, *) movieparam      ! Parameter, der def. ob ein Film geschrieben wird

   dx = (xend - x0)/(Nx - 1)    ! Schrittweite des Ortsgrids
   dy = (yend - y0)/(Ny - 1)    ! Schrittweite des Ortsgrids

   mass_h = m_h*amu      ! Reduzierte Masse des Systems
   mass_d = m_d*amu      ! Reduzierte Masse des Systems

   dt = 0.1d0/au2fs      ! Zeitschritt
   x_eq = 1.81d0            ! Gleichgewichtsabstand im Na2 Grundzustand

   laser1 = t_laser(lambda1, t_1, tp1)
   laser2 = t_laser(lambda2, t_2, tp2)

   print *, '_________________________'
   print *
   print *, 'Parameters'
   print *, '_________________________'
   print *
   print *, 'dt = ', sngl(dt*au2fs), 'fs'
   print *, 'dx = ', sngl(dx*au2a), 'A'
   print *, 'dy = ', sngl(dy*au2eV), 'A'
   print *
   print *, 'Masse H:', sngl(mass_h/amu)
   print *, 'Masse D:', sngl(mass_d/amu)
   print *, 'x_eq:', sngl(x_eq*au2a), 'A'
   print *
   print *, "Pump-Laser"
   call laser1%info()
   print *, "Dump-Laser"
   call laser2%info()
   print *
   if (movieparam .eq. 1) then
      call system('mkdir -p out/mov')
      print *, '======= Film on! =========='
   else if (movieparam .eq. 0) then
      print *, '======= Film off! ========='
   else
      print *, '===== WRONG CHOICE ========'
   end if
   print *, '___________________________'
   print *

   close (10)
end subroutine

!...................... Impulsgrid......................

subroutine p_grid

   use data_grid
   use data_au
   implicit none
   integer:: I

! In diesem Unterprogramm wird das Grid im Impulsraum definiert. Auf diesem Grid
! ist die Wellenfunktion im Impulsraum definiert - die Schrittgroesse dPR haengt
! von der Schrittgroesse im Ortsraum, dR, und der Anzahl der Gridpunkte NR zusammen.

   dPx = (2.d0*pi)/(dx*Nx)
   dPy = (2.d0*pi)/(dy*Ny)

   do I = 1, Nx
      if (I .le. (Nx/2)) then
         Px(I) = (I - 1)*dPx
      else
         Px(I) = -(Nx + 1 - I)*dpx
      end if
   end do

   do I = 1, Ny
      if (I .le. (Ny/2)) then
         Py(I) = (I - 1)*dPy
      else
         Py(I) = -(Ny + 1 - I)*dpy
      end if
   end do
   return
end subroutine

!........................... Einlesen des Potentials..............

subroutine potential

   use data_grid
   use data_au
   use pot_param

   implicit none

   integer:: i, j
   double precision:: x, y      ! Kernabstand (auf dem Grid)
   double precision:: xeq, yeq    ! Gleichgewichtsabstand
   double precision:: xeq2, yeq2    ! Gleichgewichtsabstand, ex. state
   double precision:: D, beta    ! Parameter fuer das Morse Potential
   double precision:: a, b, f    ! Parameter für das Potential, ex. state

   open (20, file='out/Ground_Pot.dat', status='unknown')
   open (21, file='out/Excited_Pot.dat', status='unknown')
   open (22, file='out/Pot_Schnitte.dat', status='unknown')

! Hier werden die Potentiale, auf dem wir die Kerndynamik berechnen wollen,
! initialisiert und alles in atomare Einheiten umgerechnet.

   D = 0.2092d0
   beta = 1.1327d0
   f = 0.00676d0
   a = 4.d-2
   b = 4.d-2
   xeq = 1.81d0
   yeq = 1.81d0
   xeq2 = 2.8d0
   yeq2 = 2.8d0

   do i = 1, nx
      do j = 1, ny
         x = x0 + (i - 1)*dx
         y = y0 + (j - 1)*dy
         pot(i, j, 1) = 0.4d0*(D*(1 - exp(-beta*(x - xeq)))**2 + D*(1 - exp(-beta*(y - yeq)))**2&
        & - f*((x - xeq)*(y - yeq))/(1.d0 + exp((x - xeq)*(y - yeq))))  ! kleiner Korrekturfaktor
         pot(i, j, 2) = 0.4d0*(9.d-1 + a*(x - xeq2)**2 + b*(y - yeq2)**2 - 10*f*(x + y))
      end do
   end do

   do i = 1, nx, 2
      do j = 1, ny, 2
         x = x0 + (i - 1)*dx
         y = y0 + (j - 1)*dy
         write (20, *) sngl(x*au2a), sngl(y*au2a), sngl(pot(i, j, 1)*au2eV)
         write (21, *) sngl(x*au2a), sngl(y*au2a), sngl(pot(i, j, 2)*au2eV)
      end do
      write (20, *)
      write (21, *)
   end do

   do i = 1, nx
      x = x0 + (i - 1)*dx
      write (22, *) sngl(x*au2a), sngl(pot(i, 64, 1)*au2eV), sngl(pot(i, 64, 2)*au2eV)
   end do

   close (20, status='keep')
   close (21, status='keep')
   close (22, status='keep')

   return
end subroutine

! ......................... Schwingungseigenfunktion ....................

subroutine nuclear_wavefct(psi0)

   use data_grid
   use pot_param
   use data_au

   implicit none

   integer:: I, J, K
   integer:: istep

   double precision:: dt2    ! Zeitschrittgroesse der imaginaeren Zeitpropagation
   double precision:: E, E1        ! Energie und Zwischenenergie zwischen 2 Zeitschritten
   double precision:: thresh    ! Konvergenzkriterium
   double precision:: x, y      ! Kernabstand (auf dem Grid)
   double precision:: x_eq, y_eq    ! Gleichgewichtsabstand
   complex*16:: norm

   complex*16, allocatable, dimension(:):: psi2, psi3
   complex*16, allocatable, dimension(:, :):: vprop, kprop! Zeitenwicklungsoperatoren
   complex*16, allocatable, dimension(:, :):: psi, psi1  ! Wellenfunktionen
   double precision, intent(out):: psi0(nx, ny)

   open (98, file='out/gaussR.dat', status='unknown')
   open (99, file='out/psi_vib.dat', status='unknown')
   open (100, file='out/Evib.dat', status='unknown')

   allocate (psi(Nx, ny), psi1(Nx, ny), vprop(Nx, ny))
   allocate (kprop(Nx, ny), psi2(ny), psi3(nx))

! In diesem Teilprogramm wirden die Eigenfunktionen der Kern-Schroedinger Gleichung
! berechnet, also die Schwingungsfunktionen. Meistens reicht allerdings nur der
! Schwingungsgrundzustand, der wie ein Gauss aussieht.

! Prinzipiell koennen mehrere Kernwellenfunktionen (die do-Schleife V-Loop) in
! mehreren elektronischen Zustaenden berechnet werden, allerdings werden wir
! erst mal nur den Grundzustand in einem elektronischen Zustand berechen.

   dt2 = dt
   thresh = 1.d-12   ! Konvergenzkriterium
   istep = 1d6    ! Anzahl der Schritte, bis Konvergenz erreicht sein muss.

   print *
   print *, 'Calculating nuclear eigenstate...'
   print *

   x_eq = 1.81d0
   y_eq = 1.81d0

   do i = 1, Nx
      do j = 1, ny
         vprop(i, j) = exp(-0.5d0*dt*pot(i, j, 1))  ! Definition des potentiellen Propagators
         kprop(i, j) = exp((-dt*(Px(i)**2/(2.0d0*mass_h) + &
             & Py(j)**2/(2.0d0*mass_d)))) ! Definition des kinetischen Propagators
      end do
   end do

   do i = 1, Nx
      x = x0 + (i - 1)*dx
      do j = 1, ny
         y = y0 + (j - 1)*dy
         psi(i, j) = exp(-1.*(x - x_eq)**2)*&   ! Anfangsfunktion: Gauss
           &      exp(-1.*(y - y_eq)**2)
         write (98, *) sngl(x*au2a), sngl(real(psi(i, j)))
      end do
      write (98, *)
   end do

   call overlap(psi, psi, norm)  ! Berechnung der Norm der Anfangswellenfunktion
   psi = psi/sqrt(abs(norm))    ! .. und normieren der Anfangswellenfunktion auf 1

   E = 0.D0        ! Energie am Anfang auf 0 setzen

!.......... Imaginary Time Propagation ........................

   do K = 1, istep

      psi1 = psi   ! Wellenfunktion des Iterationschritts (N - 1)
      E1 = E       ! Eigenwert des Iterationschritts (N - 1)

      psi = psi*vprop      ! Propagator, potentieller Teil

      do i = 1, nx
         do j = 1, ny
            psi2(j) = psi(i, j)
         end do
         call DFFT1(psi2, ny, -1)
         do j = 1, ny
            psi(i, j) = psi2(j)
         end do
      end do

      do i = 1, ny
         do j = 1, nx
            psi3(j) = psi(j, i)
         end do
         call DFFT1(psi3, nx, -1)
         do j = 1, nx
            psi(j, i) = psi3(j)
         end do
      end do

      psi = psi*kprop        ! Kinetische Propagation

      do i = 1, nx
         do j = 1, ny
            psi2(j) = psi(i, j)
         end do
         call DFFT1(psi2, ny, 1)
         do j = 1, ny
            psi(i, j) = psi2(j)
         end do
      end do

      do i = 1, ny
         do j = 1, nx
            psi3(j) = psi(j, i)
         end do
         call DFFT1(psi3, nx, 1)
         do j = 1, nx
            psi(j, i) = psi3(j)
         end do
      end do

      psi = psi/dble(nx*ny)    ! Renormieren

      psi = psi*vprop        ! Propagator, potentieller Teil

      call eigenvalue_init(psi, psi1, E, dt)    ! Berechnung der Eigenwerte
      call overlap(psi, psi, norm)      ! Berechnung der Norm

      psi = psi/sqrt(abs(norm))    ! .. und renormieren der propagierten Wellenfunktion auf 1

      if (abs(E - E1) .le. thresh) then      ! Check, ob Konvergenz erreicht ist
         goto 10
      end if

   end do

   print *, 'Iteration not converged!'
   print *, 'Program stopped!'
   print *
   print *, 'E =', E/cm2au
   print *, 'E1 =', E1/cm2au
   print *, 'thresh =', thresh/cm2au
   print *, 'step =', K

   stop

10 continue          ! Falls Konvergenz erreicht wurde, geht es hier weiter

   print *, 'Vibrational ground state energy:', sngl(E*au2eV)
   write (100, *) sngl(E*au2eV)

   do I = 1, Nx
      x = x0 + (i - 1)*dx
      do j = 1, ny
         y = y0 + (j - 1)*dy          ! Die konvergierte Wellenfunktion wird
         write (99, *) sngl(x*au2a), sngl(y*au2a), sngl(real(psi(i, j)))  ! gespeichert und rausgeschrieben.
      end do
      write (99, *)
   end do

   psi0 = real(psi)      ! Der Grundzustand

   print *

   close (98, status='keep')
   close (99, status='keep')
   close (100, status='keep')

   deallocate (psi, psi1, vprop, kprop, psi2, psi3)

   return
end

! ........................................................

subroutine eigenvalue_init(A, B, E, dt2)

   use data_grid
   implicit none
   double precision:: e1, e2
   double precision, intent(in):: dt2
   complex*16:: norm
   complex*16, intent(in):: A(nx, ny), B(nx, ny)
   double precision, intent(out):: E

! Berechnen der Eigenwerte der propagierten Wellenfunktion

   call overlap(B, B, norm)  ! Berechnet den Ueberlapp zweier Wellenfunktionen
   e1 = abs(norm)

   call overlap(A, A, norm)
   e2 = abs(norm)

   E = (-0.5d0/dt2)*log(e2/e1)

   return
end subroutine

!...............................................

subroutine overlap(A, B, norm)

   use data_grid
   implicit none
   integer:: i, j
   complex*16, intent(in):: A(Nx, ny), B(nx, ny)
   complex*16, intent(out):: norm

! Das ist der Ueberlapp zweier Wellenfunktionen

   norm = (0.d0, 0.d0)

   do i = 1, Nx
      do j = 1, Ny
         norm = norm + conjg(A(i, j))*B(i, j)
      end do
   end do

   norm = norm*dx*dy

   return
end subroutine
